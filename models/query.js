var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var QuerySchema = new Schema({
	data: {type: String } // Ответ стороннего сайта
});


module.exports = mongoose.model("Query", QuerySchema);