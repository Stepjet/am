const express = require('express');

var router = express.Router();

var request = require('request'); 

const apiKey = 'daa3123da2134'
	
var Query = require('../models/query');

var SimpleSchema = require('simpleschema');

var mqSchema = new SimpleSchema({
	token: {
		type: 'string'
		,require: true
		,notEmpty: true
	}
	,product_ids: {
		type: 'array'
		,required: true
		,notEmpty: true
	}
});


//наличие и валидность api токена
router.use(function (req, res, next) {
	let token = req.body.token ? req.body.token : req.query ? req.query.token : '';
	if (!token || token !== apiKey) {		
		return res.sendStatus(404);		
	} else {
  		next();
	}
});


//создание запроса
router.post('/query/', (req, res, next) => {
	mqSchema.validate(req.body, ( err, newBody, errors ) => {
	    if( err ){
	      next( err );
	    } else {
	    	if (errors.length){
	    		console.log('validation errors', errors)
	    		res.json({
	    			status: 'failure'
	    			,massage: 'invalid input'
	    		})
	    	} else {
				//проверяем валидность api токена
				if (newBody.token !== apiKey) {
					return res.sendStatus(404);		
				}

				//сохраняем данные в бд и возвращаем mongo_id
				var q = new Query();	
				q.save(function (err, saved) {
				  if (err) {
				    res.json({
				    	status: 'failure'
				    	,message: err
				    })
				  } else {
				    res.json({
				    	status: 'success'
				    	,id: saved._id
				    });	    
				  }
				});	

				//формируем запрос на "сторонный" сайт
				let options = {
					url: 'http://localhost:3000/remotesite'
					,method: 'POST'
					,json: {
						data: newBody.product_ids
						,token: apiKey
					}
				}

				request(options, (error, response, body) => {
					if (error) {
						console.log(error)
					}

					if (!error && response.statusCode == 200) {
						q.data = body.result;
						q.save();
					}
				});	    		
	    	}
	    }

	});

});

//данные по запросу
router.get('/data/', (req, res, next) => {
	Query.findById(req.query.id, (err, found) => {
		if (err) {
			res.json({
				status: 'failure'
				,message: err
			});
		} else {
			if (!found){
				res.json({
					status: 'failure'
					,message: 'query with provided id not found'
				})
			} else {			
				res.json({
					status: 'success'
					,data: found.data
				});
			}
		}
	});	

});

//удаление запроса
router.delete('/data/', (req, res, next) => {
	Query.findByIdAndRemove(req.query.id, (err, removed) => {
		if (err) {
			res.json({
				status: 'failure'
				,message: err
			});
		} else {
			if (!removed){
				res.json({
					status: 'failure'
					,message: 'query with provided id not found'
				})
			} else {			
				res.json({
					status: 'success'
				});
			}
		}
	});	

});


//"сторонний" сайт
router.post('/remotesite/', (req, res, next) => {
	let prods = req.body.data.join('-')
	res.json({
		status: "success"
		,result: `${prods} data`
	})

});

module.exports = router;