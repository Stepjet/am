const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const port = 3000;

var app = express();


mongoose.connect(`mongodb://user:1@localhost:27017/PRODUCTS`, (err) => {
	if (err) {
		console.log(err);
	} else {
		console.log('connected to the database');
	}
})


//Body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', require('./routes/queries'))


app.listen(port, () => console.log('server started on port ' + port));